var express = require('express');
var User = require('../app/models/user');
var jwt = require('jsonwebtoken');
var bcrypt      = require('bcrypt');

// API ROUTES -------------------
module.exports = function(appSecret) {

  const saltRounds = 10;

  var generateSalt = function(rounds, callback) {
    bcrypt.genSalt(rounds, function(err, salt) {
      if(err) {
        console.log('Error encountered while generating password salt: ' + err);
        callback(err, null);
      }

      callback(salt);
    })
  }

  var generatePasswordHash = function(salt, password, callback) {
    // 2. Hash salt+password
    // var pwd_hash = hashPassword(req.body.password, salt);
    bcrypt.hash(password, salt, function(err, hash) {
      if(err) {
        console.log('Error generating password hash: ' + err);
        callback(err, null);
      }

      callback(null, hash);
    });
  }

  var apiRoutes = express.Router();

  // Create a new user
  apiRoutes.post('/signup', function(req, res) {
    // When creating a new user:
    // #1. Generate a salt
    // #2. Hash the password using the salt.
    // #3. Save the username and password into the user document. 
    generateSalt(saltRounds, function(salt) {
      generatePasswordHash(salt, req.body.password, function(err, passwordHash) {
        if(err) {
          res.json({ success: false, message: 'An error occurred during user registration.'})
        }

        var newUser = new User({
          name: req.body.name,
          password: passwordHash,
          salt: salt
        });

        newUser.save(function(err) {
          if(err) {
            console.log('Error saving user to database: ' + err);
            res.json({ success: false, message: 'An error occurred during user registration.'})
          }

          res.json({ success: true, message: 'User created successfully.'})
        })
      })
    })
  })

  // Authenticates the token
  apiRoutes.post('/authenticate', function(req, res) {
    User.findOne({
      name: req.body.name
    }, function(err, user) {
      if(err) {
        console.log('Error authenticating user: ' + err);
        res.json({ success: false, message: 'Authentication failed: An error was encountered when attempting to authenticate the user.'});
      }

      // Unable to find matching user
      if(!user) {
        // Failed login
        res.json({ success: false, message: 'Authentication failed: Username or password is incorrect.'})
      } else {
        // Need to verify that password is correct.
        generatePasswordHash(user.salt, req.body.password, function(err, passwordHash) {
          if(err) {
            console.log('Error authenticating user: ' + err);
            res.json({ success: false, message: 'Authentication failed: An error was encountered when attempting to authenticate the user.'});
          }

          // Successful authentication
          if(passwordHash === user.password) {
            // Successful login; valid token created for 1 hour
            var token = jwt.sign({ "id" : user._id }, appSecret, {
              expiresIn: 60*60 // expires in 1 hours
            });

            res.json({ success: true, message: 'Enjoy your token!', token: token });
          } else {
            // Failed login
            res.json({ success: false, message: 'Authentication failed: Username or password is incorrect.'})
          }
        })

      }
    })
  })

  // Router middleware to verify a token
  apiRoutes.use(function(req,res,next) {
    // Check headers or url parameters for a token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    // Decode token
    if(token) {
      // Verifies secret and checks exp
      jwt.verify(token, appSecret, function(err, decoded) {
        if(err) {
          return res.json({ success: true, message: 'Failed to authenticate token.' });
        } else {
          // Check if user exists.
          User.findOne({ "_id" : decoded.id }, function(err, user) {
            if(err) return err;


            if(!user) {
              // User doesn't exist; token is invalid.
              return res.json({ success: false, message: 'Failed to authenticate token.' });
            } else {
              // User exists, add user info to decoded token
              decoded._doc = user._doc
              req.decoded = decoded;
              next();
            }
          })
        }
      });
    } else {
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      })
    }
  })

  // Route to return a random get message; greeting user by name.
  apiRoutes.get('/', function(req,res) {
    res.json('Welcome to the endpoint, ' + req.decoded._doc.name + '!');
  })

  apiRoutes.get('/account', function(req,res) {
    User.findOne({ '_id' : req.decoded.id }, function(err, user) {
      if(err) return err;

      res.send({ success:true, message: "Your account info is the following: " + JSON.stringify(user) });
    })
  });

  // Route to get all users
  apiRoutes.get('/users', function(req,res) {
    User.find({}, function(err, users) {
      res.json(users);
    })
  })

  return apiRoutes;
}
